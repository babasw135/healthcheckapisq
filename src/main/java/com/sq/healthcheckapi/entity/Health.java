package com.sq.healthcheckapi.entity;

import com.sq.healthcheckapi.enums.HealthStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Health {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate dateCreate;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private HealthStatus healthStatus;

    @Column(nullable = false)
    private Boolean isChronicDisease;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
