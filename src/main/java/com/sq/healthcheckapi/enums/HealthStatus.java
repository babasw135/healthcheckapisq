package com.sq.healthcheckapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum HealthStatus {
    BE_DEAD("죽음의 문턱", true),
    FEEL_GOOD("좋음", false),
    NORMAL("보통", false),
    SEEK("아픔", true)
    ;

    private final String name;
    private final Boolean isGoHome;
}
