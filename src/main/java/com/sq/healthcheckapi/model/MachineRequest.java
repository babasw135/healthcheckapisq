package com.sq.healthcheckapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MachineRequest {
    private String machineName;
    private Double machinePrice;
    private LocalDate dateBuy;
}
