package com.sq.healthcheckapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MachineStaticsResponse {
    private Double totalPrice;
    private Double averagePrice;
    private Double totalPressKg;
}
