package com.sq.healthcheckapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class TestRequest {
    private String name;
    private LocalDate dateIn;
}
