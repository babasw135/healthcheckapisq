package com.sq.healthcheckapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HealthResponse {
    private Long id;
    private String healthStatusName;
    private String isGoHomeName;
    private LocalDate dateCreate;
    private String name;
    private String isChronicDiseaseName;
    private String etcMemo;
    private Double resultPrice;
}
