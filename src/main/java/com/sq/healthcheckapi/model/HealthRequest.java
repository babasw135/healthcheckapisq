package com.sq.healthcheckapi.model;

import com.sq.healthcheckapi.enums.HealthStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HealthRequest {
    private String name;

    @Enumerated(value = EnumType.STRING)
    private HealthStatus healthStatus;

    private String etcMemo;

    private Boolean isChronicDisease;
}
