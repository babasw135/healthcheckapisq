package com.sq.healthcheckapi.controller;

import com.sq.healthcheckapi.model.*;
import com.sq.healthcheckapi.service.HealthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/health")
public class HealthController {
    private final HealthService healthService;

    @PostMapping("/people")
    public String setHealth(@RequestBody HealthRequest request) {
        healthService.setHealth(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<HealthItem> getHealths() {
        return healthService.getHealths();
    }

    @GetMapping("/detail/{id}")
    public HealthResponse getHealth(@PathVariable long id) {
        return healthService.getHealth(id);
    }

    @PostMapping("/test")
    public String setTest(@RequestBody TestRequest request) {
        return "OK";
    }

    @PutMapping("/status/{id}")
    public String putHealthStatus(@PathVariable long id, @RequestBody HealthStatusChangeRequest request) {
        healthService.putHealthStatus(id, request);

        return "OK";
    }

    @PutMapping("/base-info/{id}")
    public String putBaseInfo(@PathVariable long id, @RequestBody HealthBaseInfoChangeRequest request) {
        healthService.putBaseInfo(id, request);

        return "OK";
    }

    @DeleteMapping("/{id}")
    public String delHealth(@PathVariable long id) {
        healthService.delHealth(id);

        return "OK";
    }
}
