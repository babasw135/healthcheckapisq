package com.sq.healthcheckapi.controller;

import com.sq.healthcheckapi.model.MachineRequest;
import com.sq.healthcheckapi.model.MachineStaticsResponse;
import com.sq.healthcheckapi.service.MachineService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/machine")
public class MachineController {
    private final MachineService machineService;

    @PostMapping("/new")
    public String setMachine(@RequestBody MachineRequest request) {
        machineService.setMachine(request);

        return "OK";
    }

    @GetMapping("/statics")
    public MachineStaticsResponse getStatics() {
        return machineService.getStatics();
    }
}
