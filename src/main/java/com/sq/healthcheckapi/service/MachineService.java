package com.sq.healthcheckapi.service;

import com.sq.healthcheckapi.entity.Machine;
import com.sq.healthcheckapi.model.MachineRequest;
import com.sq.healthcheckapi.model.MachineStaticsResponse;
import com.sq.healthcheckapi.repository.MachineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MachineService {
    private final MachineRepository machineRepository;

    public void setMachine(MachineRequest request) {
        Machine addData = new Machine();
        addData.setMachineName(request.getMachineName());
        addData.setMachinePrice(request.getMachinePrice());
        addData.setDateBuy(request.getDateBuy());

        machineRepository.save(addData);
    }

    public MachineStaticsResponse getStatics() {
        MachineStaticsResponse response = new MachineStaticsResponse();


        List<Machine> originList = machineRepository.findAll();

        double totalPrice = 0D;
        double totalPressKg = 0D;

        for (Machine machine : originList) { // 향상된 for문
            // java 연산자
            totalPrice += machine.getMachinePrice();
            totalPressKg += machine.getMachineType().getPressKg();
            // totalPrice = totalPrice + machine.getMachinePrice();
        }


        double avgPrice = totalPrice / originList.size();

        response.setTotalPrice(totalPrice);
        response.setAveragePrice(avgPrice);
        response.setTotalPressKg(totalPressKg);

        return response;
    }
}
